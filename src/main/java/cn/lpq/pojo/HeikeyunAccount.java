package cn.lpq.pojo;

public class HeikeyunAccount {

	private String account;
	private String password;
	private int downloadCount;
	private int maxDownloadCount;
	private String isVip;

	public static HeikeyunAccount readFromLocalString(String zhinfo) {
		// 帐号:123456密码:12322456下载:0VIP:1
		String zh = zhinfo.substring(3, zhinfo.indexOf("密码:"));
		String pwd = zhinfo.substring(zhinfo.indexOf("密码:") + 3, zhinfo.indexOf("下载:"));
		int xz = Integer.valueOf(zhinfo.substring(zhinfo.indexOf("下载:") + 3, zhinfo.indexOf("VIP:")));
		String vip = zhinfo.substring(zhinfo.indexOf("VIP:") + 4);
		int maxCount = "是".equals(vip) ? 500 : 5;
		return new HeikeyunAccount(zh, pwd, xz, maxCount, vip);
	}

	public String toLocalString() {
		return "帐号:" + this.getAccount() + "密码:" + this.getPassword() + "下载:" + this.getDownloadCount() + "VIP:"
				+ this.getIsVip();
	}

	public boolean canDownload() {
		return this.getDownloadCount() < this.getMaxDownloadCount();
	}

	public HeikeyunAccount(String account, String password, int downloadCount, int maxDownloadCount, String isVip) {
		super();
		this.account = account;
		this.password = password;
		this.downloadCount = downloadCount;
		this.maxDownloadCount = maxDownloadCount;
		this.isVip = isVip;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getDownloadCount() {
		return downloadCount;
	}

	public void setDownloadCount(int downloadCount) {
		this.downloadCount = downloadCount;
	}

	public int getMaxDownloadCount() {
		return maxDownloadCount;
	}

	public void setMaxDownloadCount(int maxDownloadCount) {
		this.maxDownloadCount = maxDownloadCount;
	}

	public String getIsVip() {
		return isVip;
	}

	public void setIsVip(String isVip) {
		this.isVip = isVip;
	}

}
