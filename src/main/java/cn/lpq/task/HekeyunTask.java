package cn.lpq.task;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import cn.lpq.manage.HeikeyunManage;

@Configuration
public class HekeyunTask {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Resource
	HeikeyunManage heikeyunManage;

	@Scheduled(fixedRate = 60 * 1000)
	public void regTask() {
		if (heikeyunManage.needReg()) {
			logger.debug("当前存活数量少于配置数量，正在注册新账号");
			heikeyunManage.reg();
		}
	}

	//@Scheduled(fixedRate = 10000)
	public void testDownload() {
		List<Map<String, String>> result = heikeyunManage
				.parseMagnet("magnet:?xt=urn:btih:6066D593B4EB7DA2C9FA3040B7C17B799AB913C1",null);
		for (Map<String, String> map : result) {
			heikeyunManage.download(map.get("data"), map.get("s1"));
		}
	}

	@Scheduled(fixedRate = 120 * 1000)
	public void keepAliveTask() {
		heikeyunManage.keepAlive();
	}

}
